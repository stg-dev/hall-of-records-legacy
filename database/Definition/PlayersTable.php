<?php

/*
 * This file is part of the stg/hall-of-records package.
 *
 * (c) YTK <yutakaje@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Stg\HallOfRecords\Database\Definition;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\View;
use Stg\HallOfRecords\Shared\Infrastructure\Locale\Locales;
use Stg\HallOfRecords\Shared\Infrastructure\Type\DateTime;
use Symfony\Component\Yaml\Yaml;

/**
 * @phpstan-import-type Aliases from PlayerRecord
 */
final class PlayersTable extends AbstractTable
{
    private Connection $connection;

    public function __construct(
        Connection $connection,
        Locales $locales
    ) {
        parent::__construct($locales);
        $this->connection = $connection;
    }

    /**
     * @param AbstractSchemaManager<AbstractPlatform> $schemaManager
     */
    public function createObjects(
        AbstractSchemaManager $schemaManager,
        Schema $schema
    ): Table {
        $players = $schema->createTable('stg_players');
        $players->addColumn('id', 'integer', ['autoincrement' => true]);
        $players->addColumn('created_date', 'datetime');
        $players->addColumn('last_modified_date', 'datetime');
        $players->addColumn('name', 'string', ['length' => 100]);
        $players->addColumn('aliases', 'string', ['length' => 300]);
        $players->addColumn('name_filter', 'string', ['length' => 500]);
        $players->setPrimaryKey(['id']);
        $schemaManager->createTable($players);

        $schemaManager->createView($this->createView());

        return $players;
    }

    private function createView(): View
    {
        $qb = $this->connection->createQueryBuilder();
        $alias = 'x';

        return new View('stg_query_players', $qb->select(
            'id',
            'created_date',
            'last_modified_date',
            'name',
            'aliases',
            'name_filter',
            'num_scores'
        )
            ->from("({$this->playerSql()})", $alias)
            ->getSQL());
    }

    private function playerSql(): string
    {
        $qb = $this->connection->createQueryBuilder();

        return $qb->select(
            'players.id',
            'players.created_date',
            'players.last_modified_date',
            'players.name',
            'players.aliases',
            'players.name_filter',
            "({$this->numScoresSql()}) AS num_scores"
        )
            ->from('stg_players', 'players')
            ->getSQL();
    }

    private function numScoresSql(): string
    {
        $qb = $this->connection->createQueryBuilder();

        return $qb->select('count(*)')
            ->from('stg_scores')
            ->where($qb->expr()->eq('player_id', 'players.id'))
            ->getSQL();
    }

    /**
     * @param Aliases $aliases
     */
    public function createRecord(
        string $name,
        array $aliases = []
    ): PlayerRecord {
        return new PlayerRecord(
            $name,
            $aliases
        );
    }

    public function insertRecord(PlayerRecord $record): void
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->insert('stg_players')
            ->values([
                'created_date' => ':createdDate',
                'last_modified_date' => ':lastModifiedDate',
                'name' => ':name',
                'aliases' => ':aliases',
                'name_filter' => ':nameFilter',
            ])
            ->setParameter('createdDate', DateTime::now())
            ->setParameter('lastModifiedDate', DateTime::now())
            ->setParameter('name', $record->name())
            ->setParameter('aliases', $this->makeAliases($record))
            ->setParameter('nameFilter', $this->makeNameFilter($record))
            ->executeStatement();

        $record->setId((int)$this->connection->lastInsertId());
    }

    /**
     * @param PlayerRecord[] $records
     */
    public function insertRecords(array $records): void
    {
        $this->connection->transactional(
            function () use ($records): void {
                foreach ($records as $record) {
                    $this->insertRecord($record);
                }
            }
        );
    }

    private function makeAliases(PlayerRecord $record): string
    {
        return Yaml::dump(
            array_values($record->aliases())
        );
    }

    private function makeNameFilter(PlayerRecord $record): string
    {
        return implode('|', array_merge(
            [$record->name()],
            $record->aliases(),
        ));
    }
}
