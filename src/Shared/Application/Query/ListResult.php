<?php

/*
 * This file is part of the stg/hall-of-records package.
 *
 * (c) YTK <yutakaje@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Stg\HallOfRecords\Shared\Application\Query;

use Stg\HallOfRecords\Shared\Application\ResultMessage;

final class ListResult extends AbstractResult
{
    private Resources $resources;

    public function __construct(
        Resources $resources,
        ?ResultMessage $message = null
    ) {
        parent::__construct($message ?? ResultMessage::none());
        $this->resources = $resources;
    }

    public function resources(): Resources
    {
        return $this->resources;
    }
}
