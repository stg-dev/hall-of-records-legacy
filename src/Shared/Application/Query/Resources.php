<?php

/*
 * This file is part of the stg/hall-of-records package.
 *
 * (c) YTK <yutakaje@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Stg\HallOfRecords\Shared\Application\Query;

final class Resources
{
    /** @var Resource[] */
    private array $resources;

    /**
     * @param Resource[] $resources
     */
    public function __construct(array $resources)
    {
        $this->resources = $resources;
    }

    /**
     * @template R
     * @param \Closure(Resource):R $callback
     * @return R[]
     */
    public function map(\Closure $callback): array
    {
        return array_map($callback, $this->resources);
    }
}
